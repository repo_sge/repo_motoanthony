﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIGEFA.Entidades
{
	public class clsDocumentoIdentidad
	{
		private Int32 iCodDocumentoIdentidad;
		private Int32 iCodigoSunat;


		private Int32 iCodigoTipoDocumento;


		private String sDescripcion;
		private Int32 iLongitud;



        public Int32 CodDocumentoIdentidad
        {
            get { return iCodDocumentoIdentidad; }
            set { iCodDocumentoIdentidad = value; }
        }

        public Int32 CodigoSunat
        {
            get { return iCodigoSunat; }
            set { iCodigoSunat = value; }
        }

        public Int32 CodigoTipoDocumento
        {
            get { return iCodigoTipoDocumento; }
            set { iCodigoTipoDocumento = value; }
        }

        public String Descripcion
        {
            get { return sDescripcion; }
            set { sDescripcion = value; }
        }

        public Int32 Longitud
        {
            get { return iLongitud; }
            set { iLongitud = value; }
        }

        /*
		public int CodDocumentoIdentidad { get => iCodDocumentoIdentidad; set => iCodDocumentoIdentidad = value; }
		public int CodigoSunat { get => iCodigoSunat; set => iCodigoSunat = value; }
		public string Descripcion { get => sDescripcion; set => sDescripcion = value; }
		public int Longitud { get => iLongitud; set => iLongitud = value; }
		public int CodigoTipoDocumento { get => iCodigoTipoDocumento; set => iCodigoTipoDocumento = value; }
         * */
	}
}
