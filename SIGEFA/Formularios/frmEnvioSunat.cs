﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SIGEFA.Administradores;
using SIGEFA.Entidades;
using SIGEFA.SunatFacElec;
using System.IO;
using DevComponents.DotNetBar;
using System.Threading;
using WinApp.Comun.Dto.Intercambio;
using SIGEFA.Reportes;
//using WinApp.Comun.Dto.Modelos;
//using WinApp.Comun.Dto.Intercambio;

namespace SIGEFA.Formularios
{
    public partial class frmEnvioSunat : Office2007Form
    {

        //#region datos

        //public static Int32 iCodUser;
        //public static Int32 iCodEmpresa;
        //public static Int32 iCodSucursal;
        //public static String sEmpresa;
        //public static Int32 iCodAlmacen;
        //public static String sAlmacen;
        //public static String sUsuario = "";
        //public static String sNombreUser = "";
        //public static String sApellidoUSer = "";
        //public static Int32 iNivelUser;
        //public static String DirecIp = "";
        //public static String RUC = "";
        //public static Int32 estadoIngreso;

        //#endregion datos

        private string estado = "-1";
        private clsAdmRepositorio clsadmrepo = new clsAdmRepositorio();
        private List<clsRepositorio> lista_repositorio = null;
        private clsEmpresa empresa = null;
        private clsAdmEmpresa admemp = new clsAdmEmpresa();

        Facturacion facturacion = new Facturacion();

       // SIGEFA.SunatFacElec.Facturacion con = new SIGEFA.SunatFacElec.Facturacion();

        public frmEnvioSunat()
        {
            InitializeComponent();
        }        

        public void listar_repositorio() 
        {
            try
            {
				dg_documentos.Rows.Clear();
				dg_documentos.Refresh();
				Herramientas her = new Herramientas();

                Image imagen_pdf = Image.FromFile(@"" + her.GetResourcesPath4() + "\\pdf.png");
                Image imagen_xml = Image.FromFile(@"" + her.GetResourcesPath4() + "\\xml.png");

                lista_repositorio = new List<clsRepositorio>();
                lista_repositorio = clsadmrepo.listar_repositorio_filtrado(estado, frmLogin.iCodSucursal, frmLogin.iCodAlmacen, dtpDesde.Value, dtpHasta.Value);

                /*
                switch (modo)
				{
					case 1:
						lista_repositorio = clsadmrepo.listar_repositorio(estado, frmLogin.iCodSucursal, frmLogin.iCodAlmacen);
						break;
					case 2:
						lista_repositorio = clsadmrepo.listar_repositorio_filtrado(estado, frmLogin.iCodSucursal, frmLogin.iCodAlmacen, dtpDesde.Value, dtpHasta.Value);
						break;
				}
              */
                

                if (lista_repositorio != null)
                {
                    if (lista_repositorio.Count > 0)
                    {
                        dg_documentos.Rows.Clear();
                        foreach (clsRepositorio rep in lista_repositorio) 
                        {                            
                            dg_documentos.Rows.Add(rep.Repoid, rep.Tipodoc, rep.Tipodocumento, rep.Fechaemision, rep.Serie, 
                                                   rep.Correlativo, rep.Monto, rep.Estadosunat, rep.Mensajesunat,
                                                   imagen_xml, rep.Nombredoc, rep.Usuario, rep.Fechaemision);
                        }                        
                    }
                    totaldocs.Text = lista_repositorio.Count.ToString();
                }
                else{
                    dg_documentos.Rows.Clear();
                    totaldocs.Text = dg_documentos.Rows.Count.ToString();
                  
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }       

        public async void Envio() 
        {

            this.Cursor = Cursors.WaitCursor;
            try
            {
                //string firmaxml = "";
                string rutacertificado = "";
                string tipodocumento = "";
                string _iddocumento = "";
                string[] iddocumento = null;
                bool todocorrecto = false;


                string rutadocumento = "";
                string rutapdf = "";

                empresa = admemp.CargaEmpresa3(frmLogin.iCodEmpresa);

                if (empresa == null)
                {
                    MessageBox.Show("Registre Empresa");
                    this.Cursor = Cursors.Default;
                    return;
                }

                string rutaboletas = @"C:\DOCUMENTOS-" + empresa.Ruc + "\\DOCUMENTOS ENVIAR\\BOLETAS\\";
                string rutafactura = @"C:\DOCUMENTOS-" + empresa.Ruc + "\\DOCUMENTOS ENVIAR\\FACTURAS\\";
                string rutanotas = @"C:\DOCUMENTOS-" + empresa.Ruc + "\\DOCUMENTOS ENVIAR\\NOTAS CREDITO\\";

                if (lista_repositorio != null)
                {
                    if (lista_repositorio.Count > 0)
                    {
                        
                        rutacertificado = @"C:\DOCUMENTOS-" + empresa.Ruc + "\\CERTIFIK\\" + empresa.Certificado;

                        foreach (clsRepositorio r in lista_repositorio)
                        {
                            rutadocumento = "";
                            var TramaXmlFirmado = Convert.ToBase64String(r.Xml);

                            switch (r.Tipodoc)
                            {
                                case 1:

                                    rutadocumento = rutaboletas + r.Nombredoc + ".xml";
                                    if (!archivo_existe(rutadocumento))
                                    {
                                        File.WriteAllBytes(rutadocumento, r.Xml);
                                    }

                                    tipodocumento = "03";
                                    iddocumento = r.Nombredoc.Split('-');
                                    _iddocumento = iddocumento[2] + "-" + r.Correlativo; break;//boleta
                                case 2:

                                    rutadocumento = rutafactura + r.Nombredoc + ".xml";
                                    if (!archivo_existe(rutadocumento))
                                    {
                                        File.WriteAllBytes(rutadocumento, r.Xml);
                                    }


                                    tipodocumento = "01";
                                    iddocumento = r.Nombredoc.Split('-');
                                    _iddocumento = iddocumento[2] + "-" + r.Correlativo; break;//factura
                                case 4:

                                    rutadocumento = rutanotas + r.Nombredoc + ".xml";
                                    if (!archivo_existe(rutadocumento))
                                    {
                                        File.WriteAllBytes(rutadocumento, r.Xml);
                                    }

                                    tipodocumento = "07";
                                    iddocumento = r.Nombredoc.Split('-');
                                    _iddocumento = iddocumento[2] + "-" + r.Correlativo; break;//nota credito
                                    //_iddocumento = r.TipDocRelacion; break;//nota credito
                                case 6:
                                    tipodocumento = "08";
                                    iddocumento = r.Nombredoc.Split('-');
                                    _iddocumento = r.TipDocRelacion; break;//nota debito                            
                            }

                            //var respuestaEnvio = new EnviarDocumentoResponse();
                            EnviarDocumentoResponse rpta;

                            await facturacion.Enviar(empresa, _iddocumento, tipodocumento, TramaXmlFirmado);

                            //EnviarDocumentoResponse rpta;
                            rpta = facturacion.rpta;

                            if (rpta != null)
                            {

                                if (rpta.CodigoRespuesta == "0" && rpta.TramaZipCdr != null)
                                {
                                    r.Estadosunat = "0";
                                    r.Mensajesunat = rpta.MensajeRespuesta == null ? "Documento Enviado" : rpta.MensajeRespuesta;

                                    
                                    String ruta = @"C:\DOCUMENTOS-" + empresa.Ruc + "\\CDR\\" + "R-" + r.Nombredoc + ".zip";
                                    File.WriteAllBytes(ruta, Convert.FromBase64String(rpta.TramaZipCdr));

                                    File.WriteAllBytes($"{Program.CarpetaCdr}\\{"R-" + r.Nombredoc + ".zip"}",
                                    Convert.FromBase64String(rpta.TramaZipCdr));

                                    /*
									 * preguntar si el zip se encuentra en la ruta
									 */
                                    if (File.Exists(ruta))
                                    {
                                        r.CDR = File.ReadAllBytes(ruta);
                                    }
                                    else
                                    {
                                        r.CDR = null;
                                    }
                                    todocorrecto = clsadmrepo.actualiza_repositorio(r);
                                }
                                else
                                {
                                    r.Mensajesunat = rpta.MensajeRespuesta == null ? " " : rpta.MensajeRespuesta;


                                    if (rpta.CodigoRespuesta == "1033")
                                    {
                                        r.Estadosunat = "0";
                                        clsadmrepo.actualiza_repositorio(r);
                                    }
                                    else
                                    {

                                        if (rpta.MensajeError != null)
                                        {
                                            if (rpta.MensajeError.Contains("1033"))
                                            {
                                                r.Estadosunat = "0";
                                                clsadmrepo.actualiza_repositorio(r);
                                            }
                                            else
                                            {
                                                r.Estadosunat = "-1";
                                                if (rpta.MensajeError != "")
                                                {
                                                    r.Mensajesunat = rpta.MensajeError;
                                                }
                                        
                                                clsadmrepo.actualiza_repositorio(r);
                                            }
                                        }
                                        else
                                        {
                                            r.Estadosunat = "-1";

                                            if (rpta.MensajeError != null)
                                            {
                                                if (rpta.MensajeError != "")
                                                {
                                                    r.Mensajesunat = rpta.MensajeError;
                                                }
                                            }

                                            clsadmrepo.actualiza_repositorio(r);
                                        }
                                        
                                    }
                                }
                            }
                        }

                        if (todocorrecto)
                        {
                            MessageBox.Show("Los documentos fueron enviados de forma correcta");
                            listar_repositorio();
                            if (lista_repositorio != null)
                            {
                                totaldocs.Text = lista_repositorio.Count.ToString();
                            }
                            else
                            {
                                totaldocs.Text = "0";
                            }
                        }
                        else
                        {
                            MessageBox.Show("No todos los documentos fueron enviados de forma correcta");
                            listar_repositorio();
                        }
                    }
                }
            }
            catch (Exception a)
            {
                MessageBox.Show(a.Message);
                this.Cursor = Cursors.Default;
            }
            this.Cursor = Cursors.Default;
        }

        private void btn_envio_Click(object sender, EventArgs e)
        {
            this.Envio();
        }
       
        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmEnvioSunat_Load(object sender, EventArgs e)
        {
            cb_estado.SelectedIndex = 0; 
            listar_repositorio();            
        }

        private void cb_estado_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cb_estado.SelectedIndex == 0)
            {
                btn_envio.Enabled = true;
                estado = "-1";
            }
            else
            {

                btn_envio.Enabled = false;
                estado = "0";
            }           
            listar_repositorio();
        }

        private void frmEnvioSunat_Shown(object sender, EventArgs e)
        {
            if (lista_repositorio != null)
            {
                if (lista_repositorio.Count > 0)
                {
                    //Thread.Sleep(5000);
                    //this.Envio();
                }
            }
            else {
               
            }
        }

		private void btnConsultar_Click(object sender, EventArgs e)
		{
			listar_repositorio();
		}

        public bool archivo_existe(string archivo)
        {

            try
            {
                return (File.Exists(archivo) ? true : false);
            }
            catch (Exception) { return false; }

        }

        private void btnReporte_Click(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable("ListaRepositorio");
            // Columnas
            foreach (DataGridViewColumn column in dg_documentos.Columns)
            {
                DataColumn dc = new DataColumn(column.Name.ToString());
                dt.Columns.Add(dc);
            }
            // Datos
            for (int i = 0; i < dg_documentos.Rows.Count; i++)
            {
                DataGridViewRow row = dg_documentos.Rows[i];
                DataRow dr = dt.NewRow();
                for (int j = 0; j < dg_documentos.Columns.Count; j++)
                {
                    dr[j] = (row.Cells[j].Value == null) ? "" : row.Cells[j].Value.ToString();
                }
                dt.Rows.Add(dr);
            }

            ds.Tables.Add(dt);
            ds.WriteXml("C:\\XML\\ListaRepositorioRPT.xml", XmlWriteMode.WriteSchema);


            CRRepositorio rpt = new CRRepositorio();
            frmRptRepositorio frm = new frmRptRepositorio();
            rpt.SetDataSource(ds);
            frm.crvRepositorio.ReportSource = rpt;
            frm.Show();
        }
    }
}
