﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using SIGEFA.Administradores;
using SIGEFA.Entidades;
using Tesseract;

namespace SIGEFA.Formularios
{
    public partial class frmGestionProveedor : DevComponents.DotNetBar.Office2007Form
    {
        public Int32 Proceso = 0; //(1) Nuevo Proveedor (2)Editar Proveedor (3)Nota Ingreso
        clsAdmProveedor admProv = new clsAdmProveedor();
        public clsProveedor prov = new clsProveedor();
        clsConsultasExternas ext = new clsConsultasExternas();
        clsAdmListaPrecio AdmLista = new clsAdmListaPrecio();
        clsListaPrecio lista = new clsListaPrecio();
        Boolean margechange = false; // variable para validar si se ha realizado algun cambio en el margen de ganancia del proveedor
        clsLocalidad local = new clsLocalidad();

        clsValidar ok = new clsValidar();
        Sunat MyInfoSunat;

        public frmGestionProveedor()
        {
            InitializeComponent();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {

            this.Cursor = Cursors.WaitCursor;

            if (Proceso != 0 && txtRUC.Text != "")
            {                
                prov.Ruc = txtRUC.Text;
                prov.RazonSocial = txtRazonSocial.Text;
                prov.Direccion = txtDireccion.Text;
                prov.Telefono = txtTelefono.Text;
                prov.Fax = txtFax.Text;
                prov.Representante = txtRepresentante.Text;
                prov.Mail = txtmail.Text;
                prov.Contacto = txtContacto.Text;
                prov.TelefonoContacto = txtTelCon.Text;
                if (txtVisita.Text != "") { prov.FrecuenciaVisita = Convert.ToInt32(txtVisita.Text); }
                if (txtRecargo.Text != "") { prov.Margen = Convert.ToDouble(txtRecargo.Text); } else { prov.Margen = 0; }
                prov.Banco = txtBanco.Text;
                prov.CtaCte = txtCtaCte.Text;
                prov.Comentario = txtComentario.Text;
                prov.CodUser = frmLogin.iCodUser;
                prov.Estado = cbActivo.Checked;

                if (Proceso == 1 || Proceso == 3)
                {
                    if (admProv.insert(prov))
                    {
                        MessageBox.Show("Los datos se guardaron correctamente", "Gestion Proveedor", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                }
                else if (Proceso == 2)
                {
                    if (admProv.update(prov))
                    {
                        if (margechange)
                        {
                            DialogResult dlgResult = MessageBox.Show("Desea recalcular la listas de precios con el margen actual", "Proveedores", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (dlgResult == DialogResult.No)
                            {
                                return;
                            }
                            else
                            {
                                foreach (Int32 codlista in AdmLista.MuestraListasProveedor(frmLogin.iCodAlmacen))
                                {
                                    lista = AdmLista.CargaListaPrecio(codlista);
                                    if (AdmLista.GeneraListaProveedor(lista.CodListaPrecio, frmLogin.iCodAlmacen, lista.Decimales, prov.CodProveedor))
                                    {
                                        MessageBox.Show("Se actualizo la lista " + lista.Nombre + " ", "Gestion Proveedor", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    }
                                }
                            }
                        }
                        MessageBox.Show("Los datos se guardaron correctamente", "Gestion Proveedor", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                }
            }

            this.Cursor = Cursors.Default;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmGestionProveedor_Load(object sender, EventArgs e)
        {
            CargaLocalidades(cbDepartamento, "000000", 1);
            if (Proceso == 2)
            {
                cargaproveedor();
            }
            else if(Proceso == 3)
            {
                cargaproveedor();
                ext.sololectura(groupBox1.Controls);
                btnAceptar.Visible = false;
                btnCancelar.Text = "Aceptar";
                btnCancelar.ImageIndex = 1;
            }
        }
        private void cbDepartamento_SelectionChangeCommitted(object sender, EventArgs e)
        {
            CargaLocalidades(cbProvincia, cbDepartamento.SelectedValue.ToString(), 2);
            cbProvincia.Enabled = true;
            cbProvincia.Focus();
        }

        private void cbProvincia_SelectionChangeCommitted(object sender, EventArgs e)
        {
            CargaLocalidades(cbDistrito, cbProvincia.SelectedValue.ToString(), 3);
            cbDistrito.Enabled = true;
            cbDistrito.Focus();
        }

        private void CargaLocalidades(ComboBox Combo, String Padre, Int32 Nivel)
        {
            Combo.DataSource = local.CargaLocalidades(Padre, Nivel);
            Combo.DisplayMember = "nombre";
            Combo.ValueMember = "codLocalidad";
            Combo.SelectedIndex = -1;
        }

        private void cargaproveedor()
        {
            prov = admProv.MuestraProveedor(prov.CodProveedor);
            txtRUC.Text = prov.Ruc;
            txtRazonSocial.Text = prov.RazonSocial;
            txtDireccion.Text = prov.Direccion;
            txtTelefono.Text = prov.Telefono;
            txtFax.Text = prov.Fax;
            txtRepresentante.Text = prov.Representante;
            txtmail.Text = prov.Mail;
            txtContacto.Text = prov.Contacto;
            txtTelCon.Text = prov.TelefonoContacto;
            txtVisita.Text = prov.FrecuenciaVisita.ToString();
            txtRecargo.Text = prov.Margen.ToString();
            txtBanco.Text = prov.Banco;
            txtCtaCte.Text = prov.CtaCte;
            txtComentario.Text = prov.Comentario;
            cbActivo.Checked = prov.Estado;
            cbDepartamento.SelectedValue = prov.Departamento;
            if (prov.Departamento != "")
            {
                cbDepartamento.SelectedValue = prov.Departamento;
                CargaLocalidades(cbProvincia, prov.Departamento.ToString(), 2);
                cbProvincia.Enabled = true;

                if (prov.Provincia != "")
                {
                    cbProvincia.SelectedValue = prov.Provincia;
                    CargaLocalidades(cbDistrito, prov.Provincia.ToString(), 3);
                    cbDistrito.Enabled = true;
                    cbDistrito.SelectedValue = prov.Distrito;
                }
            }
        }

        private void txtRUC_KeyDown(object sender, KeyEventArgs e)
        {

            
        }

        private void txtRUC_KeyPress(object sender, KeyPressEventArgs e)
        {
            ok.enteros(e);
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                try
                {
                    switch (this.txtRUC.Text.Length)
                    {
                        case 1:
                            MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Solo un digito Ingresado",
                                "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                            txtRUC.SelectAll();
                            txtRUC.Focus();
                            break;

                        case 2:
                            MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Solo dos digitos Ingresados",
                                "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                            txtRUC.SelectAll();
                            txtRUC.Focus();
                            break;

                        case 3:
                            MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Solo tres digitos Ingresados",
                                "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                            txtRUC.SelectAll();
                            txtRUC.Focus();
                            break;

                        case 4:
                            MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Solo cuatro digitos Ingresados",
                                "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                            txtRUC.SelectAll();
                            txtRUC.Focus();
                            break;

                        case 5:
                            MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Solo cinco digitos Ingresados",
                                "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                            txtRUC.SelectAll();
                            txtRUC.Focus();
                            break;

                        case 6:
                            MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Solo seis digitos Ingresados",
                                "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                            txtRUC.SelectAll();
                            txtRUC.Focus();
                            break;

                        case 7:
                            MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Solo siete digitos Ingresados",
                                "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                            txtRUC.SelectAll();
                            txtRUC.Focus();
                            break;



                        case 9:
                            MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Ingreso nueve digitos ",
                                "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                            break;

                        case 10:
                            MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Ingreso diez digitos ",
                                "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                            txtRUC.SelectAll();
                            txtRUC.Focus();
                            break;

                        case 11:
                            prov = admProv.BuscaProveedor(txtRUC.Text);
                            if (prov != null)
                            {
                                MessageBox.Show("El Numero de Documento Ingresado" + Environment.NewLine + "Ya se Encuentra Registrado",
                                "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia 
                            }
                            else
                            {
                                CargarImagenSunat();
                                CargaRUC();

                            }

                            break;

                        default:
                            ValidaLongitud();
                            break;
                    }

                    //cbFamilia.Select();                    

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    CargarImagenSunat();
                }
            }
        }

        private void ValidaLongitud()
        {
            if (txtRUC.Text.Length == 0)
            {
                MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Ningun digito Ingresado",
                               "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
            }
            else if (txtRUC.Text.Length > 11)
            {
                MessageBox.Show("Ingrese Numero de Documento Valido" + Environment.NewLine + "Ha Ingresado " + txtRUC.Text.Length + " Digitos",
                               "Consulta Documento", MessageBoxButtons.OK, MessageBoxIcon.Warning);// para advertencia
                txtRUC.SelectAll();
                txtRUC.Focus();
            }
        }


        private void LeerCaptchaSunat()
        {
            //string ruta = Directory.GetCurrentDirectory()+"\\tessdata";
            //string RutaArchivo = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "tessdata\\");
            try
            {
                using (var engine = new TesseractEngine(@"./tessdata", "eng", EngineMode.Default))
                {
                    using (var image = new System.Drawing.Bitmap(pbCapchatS.Image))
                    {
                        using (var pix = PixConverter.ToPix(image))
                        {
                            using (var page = engine.Process(pix))
                            {
                                var Porcentaje = String.Format("{0:P}", page.GetMeanConfidence());
                                string CaptchaTexto = page.GetText();
                                char[] eliminarChars = { '\n', ' ' };
                                CaptchaTexto = CaptchaTexto.TrimEnd(eliminarChars);
                                CaptchaTexto = CaptchaTexto.Replace(" ", string.Empty);
                                CaptchaTexto = Regex.Replace(CaptchaTexto, "[^a-zA-Z]+", string.Empty);
                                if (CaptchaTexto != string.Empty & CaptchaTexto.Length == 4)
                                    txtSunat_Capchat.Text = CaptchaTexto.ToUpper();
                                else
                                    CargarImagenSunat();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CargarImagenSunat()
        {
            try
            {
                if (MyInfoSunat == null)
                    MyInfoSunat = new Sunat();
                this.pbCapchatS.Image = MyInfoSunat.GetCapcha;
                LeerCaptchaSunat();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void CargaRUC()
        {
            if (this.txtRUC.Text.Length == 11)
            {
                LeerDatos();
            }
        }
        private void LeerDatos()
        {
            //llamamos a los metodos de la libreria ConsultaReniec...
            MyInfoSunat.GetInfo(this.txtRUC.Text, this.txtSunat_Capchat.Text);
            switch (MyInfoSunat.GetResul)
            {
                case Sunat.Resul.Ok:
                    limpiarSunat();
                    txtRUC.Text = MyInfoSunat.Ruc;
                    txtDireccion.Text = MyInfoSunat.Direcion;
                    txtRazonSocial.Text = MyInfoSunat.RazonSocial;
                    Ciudad(MyInfoSunat.Direcion);
                    BloqueaDatos();
                    break;
                case Sunat.Resul.NoResul:
                    limpiarSunat();
                    MessageBox.Show("No Existe RUC");
                    break;
                case Sunat.Resul.ErrorCapcha:
                    limpiarSunat();
                    MessageBox.Show("Ingrese imagen correctamente");
                    break;
                default:
                    MessageBox.Show("Error Desconocido");
                    break;
            }
            //CargarImagenSunat();
        }

        private void Ciudad(string Direccion)
        {
            String[] array = Direccion.Split('-');
            if (array.Length > 1)
            {
                int a = array.Length;
                String DirTemp = array[a - 3].Trim();
                DirTemp = DirTemp.TrimEnd(' ');
                String[] ArrayDir = DirTemp.Split(' ');
                int i = ArrayDir.Length;
                //cbDepartamento.Text = ArrayDir[i - 1].Trim();
                //cbProvincia.Text = array[a - 2].Trim();
                //cbDistrito.Text = array[a - 1].Trim();
            }
        }
        private void limpiarSunat()
        {
            txtRazonSocial.Text = "";
            txtSunat_Capchat.Text = string.Empty;
        }
        private void BloqueaDatos()
        {
            /*txtRUC.ReadOnly = true; */
            txtDireccion.ReadOnly = true; txtRazonSocial.ReadOnly = true;
        }
        private void txtRecargo_TextChanged(object sender, EventArgs e)
        {
            margechange = true;
        }

        private void frmGestionProveedor_Shown(object sender, EventArgs e)
        {
            margechange = false;
            txtRUC.Focus();
        }

        private void txtRazonSocial_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtDireccion.Focus();
            }
        }

        private void txtDireccion_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                cbDepartamento.Focus();
            }
        }

        private void cbDistrito_SelectionChangeCommitted(object sender, EventArgs e)
        {
            txtTelefono.Focus();
        }

        private void txtTelefono_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtFax.Focus();
            }
        }

        private void txtFax_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtRepresentante.Focus();
            }
        }

        private void txtRepresentante_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtmail.Focus();
            }
        }

        private void txtmail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtContacto.Focus();
            }
        }

        private void txtContacto_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtTelCon.Focus();
            }
        }

        private void txtTelCon_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtVisita.Focus();
            }
        }

        private void txtVisita_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtRecargo.Focus();
            }
        }

        private void txtRecargo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtBanco.Focus();
            }
        }

        private void txtBanco_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtCtaCte.Focus();
            }
        }

        private void txtCtaCte_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtComentario.Focus();
            }
        }

        private void txtComentario_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnAceptar.Focus();
            }
        }

        private void txtRUC_KeyUp(object sender, KeyEventArgs e)
        {

        }
    }
}
