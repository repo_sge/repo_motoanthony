﻿namespace SIGEFA.Formularios
{
	partial class frmRecuperacionArchivos
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dgvVentaSinRepositorio = new System.Windows.Forms.DataGridView();
			this.codFactura = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.fecha = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.documento = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.numdoc = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.codcliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.cliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.moneda = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.total = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.formapago = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.fechapago = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.anulado = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.impreso = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.panel2 = new System.Windows.Forms.Panel();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.btnGenerarArchivos = new System.Windows.Forms.Button();
			this.dtpHasta = new System.Windows.Forms.DateTimePicker();
			this.dtpDesde = new System.Windows.Forms.DateTimePicker();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			((System.ComponentModel.ISupportInitialize)(this.dgvVentaSinRepositorio)).BeginInit();
			this.panel2.SuspendLayout();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// dgvVentaSinRepositorio
			// 
			this.dgvVentaSinRepositorio.AllowUserToAddRows = false;
			this.dgvVentaSinRepositorio.AllowUserToDeleteRows = false;
			this.dgvVentaSinRepositorio.AllowUserToResizeRows = false;
			this.dgvVentaSinRepositorio.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.dgvVentaSinRepositorio.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.dgvVentaSinRepositorio.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvVentaSinRepositorio.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.codFactura,
            this.fecha,
            this.documento,
            this.numdoc,
            this.codcliente,
            this.cliente,
            this.moneda,
            this.total,
            this.formapago,
            this.fechapago,
            this.anulado,
            this.impreso});
			this.dgvVentaSinRepositorio.Location = new System.Drawing.Point(3, 3);
			this.dgvVentaSinRepositorio.MultiSelect = false;
			this.dgvVentaSinRepositorio.Name = "dgvVentaSinRepositorio";
			this.dgvVentaSinRepositorio.ReadOnly = true;
			this.dgvVentaSinRepositorio.RowHeadersVisible = false;
			this.dgvVentaSinRepositorio.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvVentaSinRepositorio.Size = new System.Drawing.Size(1245, 307);
			this.dgvVentaSinRepositorio.TabIndex = 0;
			this.dgvVentaSinRepositorio.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvVentaSinRepositorio_CellContentClick);
			this.dgvVentaSinRepositorio.RowStateChanged += new System.Windows.Forms.DataGridViewRowStateChangedEventHandler(this.dgvVentaSinRepositorio_RowStateChanged);
			// 
			// codFactura
			// 
			this.codFactura.DataPropertyName = "codFactura";
			this.codFactura.HeaderText = "Código";
			this.codFactura.Name = "codFactura";
			this.codFactura.ReadOnly = true;
			// 
			// fecha
			// 
			this.fecha.DataPropertyName = "fecha";
			this.fecha.HeaderText = "Fecha";
			this.fecha.Name = "fecha";
			this.fecha.ReadOnly = true;
			// 
			// documento
			// 
			this.documento.DataPropertyName = "documento";
			this.documento.HeaderText = "Tipo de Documento";
			this.documento.Name = "documento";
			this.documento.ReadOnly = true;
			// 
			// numdoc
			// 
			this.numdoc.DataPropertyName = "numdoc";
			this.numdoc.HeaderText = "Número de Documento";
			this.numdoc.Name = "numdoc";
			this.numdoc.ReadOnly = true;
			// 
			// codcliente
			// 
			this.codcliente.DataPropertyName = "codcliente";
			this.codcliente.HeaderText = "RUC/DNI";
			this.codcliente.Name = "codcliente";
			this.codcliente.ReadOnly = true;
			// 
			// cliente
			// 
			this.cliente.DataPropertyName = "cliente";
			this.cliente.HeaderText = "Nombre/Razón Social";
			this.cliente.Name = "cliente";
			this.cliente.ReadOnly = true;
			// 
			// moneda
			// 
			this.moneda.DataPropertyName = "moneda";
			this.moneda.HeaderText = "Moneda";
			this.moneda.Name = "moneda";
			this.moneda.ReadOnly = true;
			// 
			// total
			// 
			this.total.DataPropertyName = "total";
			this.total.HeaderText = "Total";
			this.total.Name = "total";
			this.total.ReadOnly = true;
			// 
			// formapago
			// 
			this.formapago.DataPropertyName = "formapago";
			this.formapago.HeaderText = "Forma de Pago";
			this.formapago.Name = "formapago";
			this.formapago.ReadOnly = true;
			// 
			// fechapago
			// 
			this.fechapago.DataPropertyName = "fechapago";
			this.fechapago.HeaderText = "Fecha de Pago";
			this.fechapago.Name = "fechapago";
			this.fechapago.ReadOnly = true;
			// 
			// anulado
			// 
			this.anulado.DataPropertyName = "anulado";
			this.anulado.HeaderText = "Anulado";
			this.anulado.Name = "anulado";
			this.anulado.ReadOnly = true;
			// 
			// impreso
			// 
			this.impreso.DataPropertyName = "impreso";
			this.impreso.HeaderText = "Impreso";
			this.impreso.Name = "impreso";
			this.impreso.ReadOnly = true;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.label4);
			this.panel2.Controls.Add(this.label3);
			this.panel2.Controls.Add(this.btnGenerarArchivos);
			this.panel2.Controls.Add(this.dtpHasta);
			this.panel2.Controls.Add(this.dtpDesde);
			this.panel2.Controls.Add(this.label1);
			this.panel2.Controls.Add(this.label2);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(1251, 104);
			this.panel2.TabIndex = 2;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(941, 24);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(298, 18);
			this.label4.TabIndex = 7;
			this.label4.Text = "Seleccione una venta y haga clic en el botón";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(12, 20);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(412, 18);
			this.label3.TabIndex = 6;
			this.label3.Text = "Seleccione las fechas para filtrar las ventas que desea buscar";
			// 
			// btnGenerarArchivos
			// 
			this.btnGenerarArchivos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnGenerarArchivos.Location = new System.Drawing.Point(941, 45);
			this.btnGenerarArchivos.Name = "btnGenerarArchivos";
			this.btnGenerarArchivos.Size = new System.Drawing.Size(298, 30);
			this.btnGenerarArchivos.TabIndex = 5;
			this.btnGenerarArchivos.Text = "GENERAR ARCHIVOS";
			this.btnGenerarArchivos.UseVisualStyleBackColor = true;
			this.btnGenerarArchivos.Click += new System.EventHandler(this.btnGenerarArchivos_Click);
			// 
			// dtpHasta
			// 
			this.dtpHasta.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.dtpHasta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtpHasta.Location = new System.Drawing.Point(310, 45);
			this.dtpHasta.Name = "dtpHasta";
			this.dtpHasta.Size = new System.Drawing.Size(114, 26);
			this.dtpHasta.TabIndex = 3;
			this.dtpHasta.ValueChanged += new System.EventHandler(this.dtpHasta_ValueChanged);
			// 
			// dtpDesde
			// 
			this.dtpDesde.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.dtpDesde.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.dtpDesde.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtpDesde.Location = new System.Drawing.Point(84, 45);
			this.dtpDesde.Name = "dtpDesde";
			this.dtpDesde.Size = new System.Drawing.Size(114, 26);
			this.dtpDesde.TabIndex = 2;
			this.dtpDesde.ValueChanged += new System.EventHandler(this.dtpDesde_ValueChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(11, 50);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(70, 20);
			this.label1.TabIndex = 0;
			this.label1.Text = "DESDE:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(237, 50);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(67, 20);
			this.label2.TabIndex = 1;
			this.label2.Text = "HASTA:";
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.dgvVentaSinRepositorio);
			this.panel1.Location = new System.Drawing.Point(0, 110);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(1251, 313);
			this.panel1.TabIndex = 3;
			// 
			// frmRecuperacionArchivos
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1251, 435);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.panel2);
			this.DoubleBuffered = true;
			this.Name = "frmRecuperacionArchivos";
			this.Text = "Generación de PDF y XML";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.Load += new System.EventHandler(this.frmRecuperacionArchivos_Load);
			((System.ComponentModel.ISupportInitialize)(this.dgvVentaSinRepositorio)).EndInit();
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.DataGridView dgvVentaSinRepositorio;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.DateTimePicker dtpHasta;
		private System.Windows.Forms.DateTimePicker dtpDesde;
		private System.Windows.Forms.Button btnGenerarArchivos;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.DataGridViewTextBoxColumn codFactura;
		private System.Windows.Forms.DataGridViewTextBoxColumn fecha;
		private System.Windows.Forms.DataGridViewTextBoxColumn documento;
		private System.Windows.Forms.DataGridViewTextBoxColumn numdoc;
		private System.Windows.Forms.DataGridViewTextBoxColumn codcliente;
		private System.Windows.Forms.DataGridViewTextBoxColumn cliente;
		private System.Windows.Forms.DataGridViewTextBoxColumn moneda;
		private System.Windows.Forms.DataGridViewTextBoxColumn total;
		private System.Windows.Forms.DataGridViewTextBoxColumn formapago;
		private System.Windows.Forms.DataGridViewTextBoxColumn fechapago;
		private System.Windows.Forms.DataGridViewTextBoxColumn anulado;
		private System.Windows.Forms.DataGridViewTextBoxColumn impreso;
		private System.Windows.Forms.Panel panel1;
	}
}