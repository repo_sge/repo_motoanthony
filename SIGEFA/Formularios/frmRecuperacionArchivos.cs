﻿using SIGEFA.Administradores;
using SIGEFA.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SIGEFA.Formularios
{
	public partial class frmRecuperacionArchivos : DevComponents.DotNetBar.Office2007Form
	{

		//SIGEFA.SunatFacElec.Conexion conex = new SunatFacElec.Conexion();
		clsAdmFacturaVenta AdmVenta = new clsAdmFacturaVenta();
		clsAdmCliente AdmCli = new clsAdmCliente();
		clsAdmSerie Admser = new clsAdmSerie();
		clsAdmTransaccion AdmTran = new clsAdmTransaccion();
		clsAdmGuiaRemision AdmGuia = new clsAdmGuiaRemision();
		public static BindingSource data = new BindingSource();
		String filtro = String.Empty;
		clsFacturaVenta venta = new clsFacturaVenta();
		clsSerie ser = new clsSerie();
		clsTransaccion tran = new clsTransaccion();
		clsFacturaVenta ventaConsultada = new clsFacturaVenta();
		clsCliente cli = new clsCliente();
		clsGuiaRemision guia = new clsGuiaRemision();
		public List<clsDetalleFacturaVenta> ListaDetalleVenta = new List<clsDetalleFacturaVenta>();
		DataTable detalleTableVenta;

		/**
		 * Variables de tipos primitivos
		 */
		Int32 NumeroDocumento;

		public frmRecuperacionArchivos()
		{
			InitializeComponent();
		}

		private void frmRecuperacionArchivos_Load(object sender, EventArgs e)
		{

		}

		private void dtpDesde_ValueChanged(object sender, EventArgs e)
		{
			CargaLista();
		}

		private void dtpHasta_ValueChanged(object sender, EventArgs e)
		{
			CargaLista();
		}

		private void CargaLista()
		{
			dgvVentaSinRepositorio.DataSource = data;
			data.DataSource = AdmVenta.VentaSinRepositorio(frmLogin.iCodAlmacen, dtpDesde.Value, dtpHasta.Value);
			data.Filter = String.Empty;
			filtro = String.Empty;
			dgvVentaSinRepositorio.ClearSelection();
		}

		private void btnGenerarArchivos_Click(object sender, EventArgs e)
		{
			try
			{
				if (dgvVentaSinRepositorio.Rows.Count >= 1 && dgvVentaSinRepositorio.CurrentRow != null)
				{
					if (venta.CodFacturaVenta != "")
					{

						//MessageBox.Show("Codigo Venta: " + venta.CodFacturaVenta + " Codigo Cliente: " + NumeroDocumento, "Mensaje");
						/**
						 * Cargar datos de cliente, venta y detalle venta
						 */
						ventaConsultada = AdmVenta.CargaFacturaVenta(Convert.ToInt32(venta.CodFacturaVenta));

						if (ventaConsultada != null)
						{
							/*ser = Admser.MuestraSerie(ventaConsultada.CodSerie, frmLogin.iCodAlmacen);
							guia = AdmGuia.CargaGuiaVenta(Convert.ToInt32(ventaConsultada.CodFacturaVenta));*/

							CargaTransaccion(ventaConsultada.CodTipoTransaccion);


							cli = AdmCli.MuestraCliente(ventaConsultada.CodCliente);
							
							CargaDetalle();
							RecorreDetalle();

							/*
							 * Agregar codigo empresa
							 */
							ventaConsultada.CodEmpresa = frmLogin.iCodEmpresa;
							//conex.GeneraXMLSinEnvio(cli, ventaConsultada, ListaDetalleVenta);
							CargaLista();
						}
						else
						{
							MessageBox.Show("El documento solicitado no existe", "Venta", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
						
					}

				}
			}
			catch (Exception ex)
			{
				return;
			}
			
		}

		private void dgvVentaSinRepositorio_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
		{
			if (dgvVentaSinRepositorio.Rows.Count >= 1 && e.Row.Selected)
			{
				venta.CodFacturaVenta = e.Row.Cells[codFactura.Name].Value.ToString();
				NumeroDocumento = Convert.ToInt32(e.Row.Cells[codcliente.Name].Value);
			}
		}

		private void CargaDetalle()
		{
			detalleTableVenta = AdmVenta.CargaDetalle(Convert.ToInt32(venta.CodFacturaVenta), frmLogin.iCodAlmacen);
		}

		private void RecorreDetalle()
		{
			if (detalleTableVenta.Rows.Count > 0)
			{
				foreach (DataRow row in detalleTableVenta.Rows)
				{
					añadedetalle(row);
				}
			}
		}

		private void añadedetalle(DataRow fila)
		{
			clsDetalleFacturaVenta deta = new clsDetalleFacturaVenta();
			deta.CodProducto = Convert.ToInt32(fila["codProducto"]);
			deta.CodVenta = Convert.ToInt32(venta.CodFacturaVenta);
			deta.CodAlmacen = frmLogin.iCodAlmacen;
			deta.UnidadIngresada = Convert.ToInt32(fila["codUnidadMedida"]);
			deta.SerieLote = "";
			deta.Cantidad = Convert.ToDouble(fila["cantidad"]);
			deta.PrecioUnitario = Convert.ToDouble(fila["preciounitario"]);
			deta.Subtotal = Convert.ToDouble(fila["subtotal"]);
			deta.Descuento1 = Convert.ToDouble(fila["descuento1"]);
			deta.MontoDescuento = Convert.ToDouble(fila["montodscto"]);
			deta.Igv = Convert.ToDouble(fila["igv"]);
			deta.Importe = Convert.ToDouble(fila["importe"]);
			deta.PrecioReal = Convert.ToDouble(fila["precioreal"]);
			deta.ValoReal = Convert.ToDouble(fila["valoreal"]);
			deta.CodUser = frmLogin.iCodUser;
			deta.CantidadPendiente = Convert.ToDouble(fila["cantidad"]);
			deta.Moneda = 1;
			deta.Descripcion = fila["producto"].ToString();
			deta.CodTipoArticulo = 1;
			deta.Tipoimpuesto = fila["tipoimpuesto"].ToString();
			deta.Entregado = true;
			deta.TipoUnidad = 1;
			deta.CodDetalleCotizacion = 0;
			deta.CodDetallePedido = Convert.ToInt32(fila["codDetalle"]);
				
			ListaDetalleVenta.Add(deta);
		}

		private void CargaTransaccion(Int32 CodTransaccion)
		{
			tran = AdmTran.MuestraTransaccion(CodTransaccion);
			tran.Configuracion = AdmTran.MuestraConfiguracion(tran.CodTransaccion);
		}

		private void dgvVentaSinRepositorio_CellContentClick(object sender, DataGridViewCellEventArgs e)
		{

		}
	}
}
