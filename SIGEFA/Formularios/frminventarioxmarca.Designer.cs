﻿namespace SIGEFA.Formularios
{
    partial class frminventarioxmarca
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frminventarioxmarca));
            this.cmbimarca = new System.Windows.Forms.ComboBox();
            this.btnreportemarca = new System.Windows.Forms.Button();
            this.btnsalir = new System.Windows.Forms.Button();
            this.dgvimarca = new System.Windows.Forms.DataGridView();
            this.btnfiltrarmarca = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvimarca)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbimarca
            // 
            this.cmbimarca.FormattingEnabled = true;
            this.cmbimarca.Location = new System.Drawing.Point(65, 37);
            this.cmbimarca.Name = "cmbimarca";
            this.cmbimarca.Size = new System.Drawing.Size(203, 21);
            this.cmbimarca.TabIndex = 0;
            this.cmbimarca.SelectedIndexChanged += new System.EventHandler(this.cmbimarca_SelectedIndexChanged);
            // 
            // btnreportemarca
            // 
            this.btnreportemarca.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnreportemarca.Image = ((System.Drawing.Image)(resources.GetObject("btnreportemarca.Image")));
            this.btnreportemarca.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnreportemarca.Location = new System.Drawing.Point(584, 365);
            this.btnreportemarca.Name = "btnreportemarca";
            this.btnreportemarca.Size = new System.Drawing.Size(123, 38);
            this.btnreportemarca.TabIndex = 2;
            this.btnreportemarca.Text = "Reporte";
            this.btnreportemarca.UseVisualStyleBackColor = true;
            this.btnreportemarca.Click += new System.EventHandler(this.btnreportemarca_Click);
            // 
            // btnsalir
            // 
            this.btnsalir.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsalir.Image = ((System.Drawing.Image)(resources.GetObject("btnsalir.Image")));
            this.btnsalir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsalir.Location = new System.Drawing.Point(346, 365);
            this.btnsalir.Name = "btnsalir";
            this.btnsalir.Size = new System.Drawing.Size(105, 38);
            this.btnsalir.TabIndex = 3;
            this.btnsalir.Text = "Salir";
            this.btnsalir.UseVisualStyleBackColor = true;
            this.btnsalir.Click += new System.EventHandler(this.btnsalir_Click);
            // 
            // dgvimarca
            // 
            this.dgvimarca.AllowUserToAddRows = false;
            this.dgvimarca.AllowUserToDeleteRows = false;
            this.dgvimarca.AllowUserToOrderColumns = true;
            this.dgvimarca.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvimarca.Location = new System.Drawing.Point(41, 81);
            this.dgvimarca.Name = "dgvimarca";
            this.dgvimarca.ReadOnly = true;
            this.dgvimarca.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvimarca.Size = new System.Drawing.Size(685, 267);
            this.dgvimarca.TabIndex = 4;
            // 
            // btnfiltrarmarca
            // 
            this.btnfiltrarmarca.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnfiltrarmarca.Image = ((System.Drawing.Image)(resources.GetObject("btnfiltrarmarca.Image")));
            this.btnfiltrarmarca.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnfiltrarmarca.Location = new System.Drawing.Point(322, 28);
            this.btnfiltrarmarca.Name = "btnfiltrarmarca";
            this.btnfiltrarmarca.Size = new System.Drawing.Size(100, 36);
            this.btnfiltrarmarca.TabIndex = 5;
            this.btnfiltrarmarca.Text = "Filtrar";
            this.btnfiltrarmarca.UseVisualStyleBackColor = true;
            this.btnfiltrarmarca.Click += new System.EventHandler(this.btnfiltrarmarca_Click);
            // 
            // frminventarioxmarca
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(771, 434);
            this.Controls.Add(this.btnfiltrarmarca);
            this.Controls.Add(this.dgvimarca);
            this.Controls.Add(this.btnsalir);
            this.Controls.Add(this.btnreportemarca);
            this.Controls.Add(this.cmbimarca);
            this.DoubleBuffered = true;
            this.Name = "frminventarioxmarca";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frminventarioxmarca_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvimarca)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbimarca;
        private System.Windows.Forms.Button btnreportemarca;
        private System.Windows.Forms.Button btnsalir;
        private System.Windows.Forms.DataGridView dgvimarca;
        private System.Windows.Forms.Button btnfiltrarmarca;
    }
}