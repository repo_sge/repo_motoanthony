﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SIGEFA.Entidades;
using SIGEFA.Administradores;
using SIGEFA.Interfaces;
using SIGEFA.InterMySql;
 
namespace SIGEFA.Formularios
{
    public partial class frminventarioxmarca : DevComponents.DotNetBar.Office2007Form
    {
        clsMarca rmarca = new clsMarca();
        clsAdmMarca admmarca = new clsAdmMarca();


        public frminventarioxmarca()
        {

            InitializeComponent();
        }

        private void frminventarioxmarca_Load(object sender, EventArgs e)
        {
            CargaLista();
            //ConsultaArbol();
            //llenaarbol(0,0, null);
            //label7.Text = "Referencia";
            //label6.Text = "referencia";
        }
        private void CargaLista()
        {
            //this.Cursor = Cursors.WaitCursor;

            cmbimarca.DataSource = admmarca.MuestraMarcas();
            cmbimarca.DisplayMember = "descripcion";
            cmbimarca.ValueMember = "CodMarca";
        }

        private void btnfiltrarmarca_Click(object sender, EventArgs e)
        {
           int codmarca =Convert.ToInt32( cmbimarca.SelectedValue);

            dgvimarca.DataSource = admmarca.ListaProductoMarca(codmarca);

        }

        private void btnreportemarca_Click(object sender, EventArgs e)
        {
            try
            {
                rptInventariomarca frm = new rptInventariomarca();
                frm.codmarca= Convert.ToInt32(cmbimarca.SelectedValue);
                //frm.costo = cbCosto.Checked;
                //frm.art = cbArticulos.Checked;
                //frm.fam = cbFamilias.Checked;
                //frm.lin = cbLineas.Checked;
                //frm.gru = cbGrupos.Checked;
                //frm.tip = cbTipos.Checked;
                //frm.todo = rbTodos.Checked;
                //frm.art1 = codArticulo1;
                //frm.art2 = codArticulo2;
                //frm.cero = cbCero.Checked;
                //frm.activos = cbActivos.Checked;
                //frm.orden = Convert.ToInt32(cmbOrden.SelectedIndex);
                frm.ShowDialog();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Reporte_Inventario"); }
        }

        private void btnsalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmbimarca_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
