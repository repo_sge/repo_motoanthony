﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SIGEFA.Reportes.clsReportes;
using SIGEFA.Reportes;

namespace SIGEFA.Formularios
{
    public partial class rptInventariomarca : Form
    {
        public Int32 codmarca;
        DataSet data = null;
        public rptInventariomarca()
        {
            InitializeComponent();
        }

        private void rptInventariomarca_Load(object sender, EventArgs e)
        {
            generareporte_xmarca();
        }
        private void generareporte_xmarca()
        {
            try
            {
                clsReportProductos re = new clsReportProductos();

                data = re.Inventario_xmarca(codmarca);
                CRInventarioMarca myDataReport = new CRInventarioMarca();
                myDataReport.SetDataSource(data.Tables[0].DefaultView);
                crystalReportViewer1.ReportSource = myDataReport;
            }
            catch (Exception ex)
            {
                DevComponents.DotNetBar.MessageBoxEx.Show("Se encontró el siguiente problema: " + ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
    }
}
