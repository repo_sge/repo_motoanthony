﻿using SIGEFA.Administradores;
using SIGEFA.Formularios;
using SIGEFA.Reportes.clsReportes;
using System;
using System.Data;
using System.Windows.Forms;

namespace SIGEFA.Reportes
{
    public partial class frmParamDocumentosEnviados : DevComponents.DotNetBar.Office2007Form
    {

        clsAdmTipoDocumento admTipoDocumento = new clsAdmTipoDocumento();
        clsReporteDocumentosEnviados ds = new clsReporteDocumentosEnviados();
        public static BindingSource data = new BindingSource();
        String filtro = String.Empty;

        public frmParamDocumentosEnviados()
        {
            InitializeComponent();
        }

        private void frmParamDocumentosEnviados_Load(object sender, EventArgs e)
        {
            CargaTipoDocumentosElectronicos();
        }

        private void btnReporte_Click(object sender, EventArgs e)
        {
            BuscarVentas();
        }

        private void CargaTipoDocumentosElectronicos()
        {
            cboTipoDocumento.DataSource = admTipoDocumento.MuestraTipoDocumentosElectronicos();
            cboTipoDocumento.DisplayMember = "descripcion";
            cboTipoDocumento.ValueMember = "codTipoDocumento";
            cboTipoDocumento.SelectedIndex = 0;
        }

        private void BuscarVentas()
        {
            CRDocumentosEnviados rpt = new CRDocumentosEnviados();
            frmRptDocumentosEnviados frm = new frmRptDocumentosEnviados();

            DataTable dt = ds.DocumentosEnviados(frmLogin.iCodAlmacen, dtpDesde.Value, dtpHasta.Value,
                                                 Convert.ToInt32(cboTipoDocumento.SelectedValue)).Tables[0];
            if (dt.Rows.Count > 0)
            {
                rpt.SetDataSource(dt);
                frm.crvRptDocumentosEnviados.ReportSource = rpt;
                frm.Show();
            }
            else
            {
                MessageBox.Show("No se ha encontrado resultados con los filtros seleccionados",
                                "Reporte", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
