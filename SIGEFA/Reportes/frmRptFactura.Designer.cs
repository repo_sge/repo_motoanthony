﻿namespace SIGEFA.Reportes
{
    partial class frmRptFactura
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.crvReporteFactura = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.CRReporteFactura1 = new SIGEFA.Reportes.CRReporteFactura();
            this.CRReporteFactura2 = new SIGEFA.Reportes.CRReporteFactura();
            this.CRReporteFactura3 = new SIGEFA.Reportes.CRReporteFactura();
            this.CRReporteFactura4 = new SIGEFA.Reportes.CRReporteFactura();
            this.SuspendLayout();
            // 
            // crvReporteFactura
            // 
            this.crvReporteFactura.ActiveViewIndex = 0;
            this.crvReporteFactura.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crvReporteFactura.Cursor = System.Windows.Forms.Cursors.Default;
            this.crvReporteFactura.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crvReporteFactura.Location = new System.Drawing.Point(0, 0);
            this.crvReporteFactura.Name = "crvReporteFactura";
            this.crvReporteFactura.ReportSource = this.CRReporteFactura4;
            this.crvReporteFactura.Size = new System.Drawing.Size(387, 233);
            this.crvReporteFactura.TabIndex = 0;
            this.crvReporteFactura.Load += new System.EventHandler(this.crvReporteFactura_Load);
            // 
            // frmRptFactura
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(387, 233);
            this.Controls.Add(this.crvReporteFactura);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmRptFactura";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmRptFactura";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);

        }

        #endregion

        public CrystalDecisions.Windows.Forms.CrystalReportViewer crvReporteFactura;
        private CRReporteFactura CRReporteFactura1;
        private CRReporteFactura CRReporteFactura2;
        private CRReporteFactura CRReporteFactura3;
        private CRReporteFactura CRReporteFactura4;
    }
}